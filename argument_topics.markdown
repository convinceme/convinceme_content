| Topic                                          | Categories                 | Specificity   | Location |
|------------------------------------------------|----------------------------|---------------|----------|
| Require Abstinence-Only Education              | Health, Sex, Education     | 2             |          |
| Prevent the Dakota Access Pipeline (DAPL)      | Energy, Oil, Environment   | 4             |          |
| Raise Minimum Wage to $15 an Hour              | Labor                      | 1             |          |
| Require Background Checks on All Gun Purchases | Gun Control, Violence      | 1             |          |
| Penalize Sanctuary Cities                      | Immigration                | 3             |          |
| Require Free Tuition at Public Colleges and Universities | Student Loans    | 2             |          |
| Outlaw the Death Penalty                       | Criminal Justice, Prison   | 1             |          |
| Create a Carbon Tax                            | Energy, Environment        | 2             |          |
| Police With Stop and Frisk                     | Criminal Justice, Policing, Race | 3       |          |
| Provide School Vouchers                        | Education                  | 3             |          |
| Decriminalize Marijuana Use                    | Drugs, Medicine, Criminal Justice | 1      |          |
| Pull out of Paris Agreement                    | Environment, Climate       | 3             |          |
| Legalize Rent Control                          | Housing                    | 5             | Chicago, IL |
| Defund US Department of Housing and Urban Development (HUD) | Housing       | 4             |          |
| Eliminate Cash Bail                            | Criminal Justice, Policing | 3             |          |
| Reduce Estate Tax                              | Taxation                   | 3             |          |
| Raise Taxes on People Who Make Over $500,000 a Year | Taxation              | 3             |          |
| Remove Common Core                             | Education                  | 4             |          |
| Raise Taxes on Corporations                    | Finance, Corporations, Taxation | 1        |          |
| Repeal Dodd Frank                              | Finance, Corporations      | 4             |          |
| Expand Earned Income Tax Credit                | Taxation, Finance          | 4             |          |
| Provide Universal Pre-K                        | Education                  | 3             |          |
| Repeal Every Student Succeeds Act              | Education                  | 4             |          |
| Defund Federal Department of Education         | Education                  | 3             |          |
| Prevent Fracking                               | Energy, Environment        | 3             |          |
| Subsidize Clean Energy                         | Energy, Environment        | 2             |          |