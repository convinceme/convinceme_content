| Category | Parent categories |
|----------|-------------------|
| Criminal Justice |  |
| Education |  |
| Environment |  |
| Finance |  |
| Gender |  |
| Gun Control |  |
| Health |  |
| Immigration |  |
| Labor |  |
| Medicine | Health |
| Drugs |  |
| Race |  |
| Religion |  |
| Sex |  |
| Technology |  |
| Housing |  |
| Corporations |  |
| Campaign Finance | Finance |
| Oil | Energy |
| Climate | Environment |
| Energy | Environment |
| Policing | Criminal Justice |
| Prison | Criminal Justice |
| Violence | Criminal Justice |
| Feminism | Gender |
| Student Loans | Education, Finance |
| Rent Control | Housing |
| Taxation | Finance |
| LGBTQ | Gender |