# Welcome to [ConvinceMe](https://convinceme.us)

_Think about what you actually want the government to do&mdash;contrast it with what other people want_

- Topics are actions the government can take&mdash;not ideas
- Responses are restricted to 1400 characters<sup>[1](#footnotes)</sup>
- Information can be improved by everyone (via merge requests)

See the rest of [the contributing guidelines](https://gitlab.com/convinceme/convinceme_content/blob/master/CONTRIBUTING.markdown).

_"I would have written a shorter letter, but I did not have the time"_ -Blaise Pascal

## Goals/Future

Since you give your opinion on concrete government actions, we'll be able to connect with voting records - and provide an easy way to choose representatives who are aligned _with your actual viewpoints_.

America has [a critical lack of faith in its political representation](http://fortune.com/2017/01/25/us-democracy-downgrade/), for good reason&mdash;our representation [is mostly funded by lobbyists](https://allaregreen.us).

This seems like an ideal way to give people a chance to consider arguments, select the ones they agree with and be given a list they can take to polls and use to select representation which represents their interests&mdash;not just the one with the biggest media campaign.

## Government Changes & Categories

The changes the government could make and the categories for those actions are stored in this repository.

You can recommend additions by creating a merge-request updating [argument_topics.tsv](argument_topics.tsv).

Changes can be tagged with various categories (which must be included in the [argument_categories.tsv](argument_categories.tsv))

## Internet points

Preliminary point system:

`Number of times voted` + (`contributions` - `number of rejected merge requests`) * `contribution modifier`

The objectives being: encouraging contribution, reading (and voting), and not suggesting bad/inconsequential changes.

_"If I have learned anything from the Internet, it is this: be very, very careful when you put a number next to someone's name"_ -[Jeff Atwood](https://blog.codinghorror.com/because-reading-is-fundamental-2/)

## Sweet icon source

<img src="assets/icon.png" height="40"> [Debate by Gregor Cresnar from Noun Project](https://thenounproject.com/term/debate/419942/)

-----------------------------------------------------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"/></a>

The content of all ConvinceMe projects is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>. The source code for ConvinceMe is licensed under [GPL 3](LICENSE).

Please also look at our [Code of Conduct](code_of_conduct.markdown)


-----------------------------------------------------
### Footnotes

1. _Why 1400 characters?_ Because it's longer than a tweet. And it seemed like a reasonable place to start - better to be too short and have to loosen restrictions later than be too long.
