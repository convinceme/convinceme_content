Good arguments do these things:

- Explain what the change would do
- Explain why or why not a change should happen
- Use references

The goal is to convince people, not preach to the choir or insult people who believe different things

---

**If you haven't used GitHub before, check out the [step by step guide to suggesting improvements](using_github.markdown)**

- Use [markdown text formatting](https://docs.gitlab.com/ee/user/markdown.html/)
- Do not include images
- Inline links are only permitted for linking to other ConvinceMe pages
- Use reference footnotes for all external sources (see the __References__ section below)
- Keep body character count below 1400 characters (see the __Character Limit__ section below)
- It __must__ be made clear if something is a projection, estimate or supposition

When you open (or make changes to) a pull request on one of the Argument repositories, ConvinceMe checks your changes. You can follow the link to preview the changes that you made to the arguments.

While It's processing, it will look like this:

![Pending CI](assets/ci_pending.png)

If your changes pass, it will turn green:

... image will be here soon!

If they don't pass, it will try to tell you why they didn't. You can click on it to see what caused the error.

![Pending CI](assets/ci_fail.png)

## References

The list of the usable references is included at the top of the markdown document (in the yaml [front matter](https://jekyllrb.com/docs/frontmatter/)):

```
---
references:
  -
    title: First reference!
    link: https://www.example.com
    author: Somebody
    publisher: A publisher
    date_published: 2017-02-24
  -
    title: An article about ConvinceMe
    link: http://www.example.com/opinions
    author: 
    publisher: 
    date_published:

---
```

To cite a reference, put the name of the source in a `<ref></ref>` tag.

For example, to cite the second of the above examples, you would put `<ref>An article about ConvinceMe</ref>`.

__Starting a new argument?__ [Copy the template](new_argument_template.markdown) to get the front matter and an empty reference.

If there's already a reference block, copy and paste this empty reference (before the second `---`), and fill it in with the appropriate data (skip any fields you don't have)

```
  -
    title: 
    link: 
    author: 
    publisher: 
    date_published: 
```

__Reference fields:__

- __title__ _Required_
- __links__ _Required_
- __author__ The name the author chooses to identify with (e.g. if they publish using a pseudonym or handle, use that)
- __publisher__ Name of the publisher
- __date_published__ Date published. Use YYYY-MM-DD ([ISO 8601 format](https://en.wikipedia.org/wiki/ISO_8601))

## Character Limit

The character count that is compared to the 1400 character limit is the number of _rendered_ characters.

- It does not include the references section
- It does not include characters between the `<ref></ref>` tags (or any other HTML tags)
- It doesn't include formatting characters

---------------------------------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"/></a>

The content of all ConvinceMe projects is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>. The source code for ConvinceMe is licensed under [GPL 3](https://gitlab.com/convinceme/convinceme_content/blob/master/LICENSEhttps://docs.gitlab.com/ee/user/markdown.html).

Any contributed content will be licensed under the licenses that govern the project.