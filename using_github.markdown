### Step by Step guide to suggesting improvements

GitHub is a site that stores code; it changed how software was built and is pretty awesome. It keeps track of the changes to files in _repositories_ (collections of files), and gives programmers powerful tools for managing and moderating changes. It can be a little confusing for the uninitiated.

__If you're looking for instructions for making your first change on GitHub, read on__.

You have to create an account on GitHub. Once you've done that, follow these instructions. Basically, you suggest a change, and once one of the editors approves it, it will show up on the site.

_Note: this requires the desktop version of GitHub.com. If you're on a phone, click the "Desktop version" link at the bottom of the page and you can follow along._

-----------------------------------------------------

After clicking an "Improve this argument" link on [ConvinceMe.us](https://www.convinceme.us), if you're signed in to GitHub, you'll see a screen that looks like this:

![First step](assets/contributing_1st_step.png)

Click the green button.

Then you'll see a screen that looks like this:

![Second step](assets/contributing_2nd_step.png)

You should make the changes you want to make to the argument in the big text box. There are some guidelines (and information about citations) for contributing that you can [read here](https://www.convinceme.us/content_instructions).

Once you've made the changes, click the green button at the bottom of the page. You will be taken to a new page.


![Third step](assets/contributing_3rd_step.png)

Click the green button.


You'll see this:

![Fourth step](assets/contributing_4th_step.png)

Click the green button. You've suggested your change! It's called a _pull request_.

If the editors have any comments about your change, they'll make them on GitHub and you'll receive an email notifying you.

Otherwise, it will be _merged_ in to the existing code and will show up on ConvinceMe.us in a few minutes.